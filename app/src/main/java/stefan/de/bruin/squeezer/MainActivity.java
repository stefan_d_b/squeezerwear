package stefan.de.bruin.squeezer;

import android.app.Activity;

import android.app.ActionBar;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import stefan.de.bruin.squeezer.Dialogs.AuthenticationDialog;
import stefan.de.bruin.squeezer.Dialogs.EnableWifiDialog;
import stefan.de.bruin.squeezer.Models.PlayerModel;
import stefan.de.bruin.squeezer.Models.PlayerStateModel;
import stefan.de.bruin.squeezer.fragments.DisconnedtedFragment;
import stefan.de.bruin.squeezer.framework.BaseSqueezer;
import stefan.de.bruin.squeezer.listitems.AlbumListFragment;
import stefan.de.bruin.squeezer.listitems.ApplicationListFragment;
import stefan.de.bruin.squeezer.listitems.ArtistListFragment;
import stefan.de.bruin.squeezer.listitems.CurrentPlaylistFragment;
import stefan.de.bruin.squeezer.listitems.FavoritesListFragment;
import stefan.de.bruin.squeezer.listitems.GenresListFragment;
import stefan.de.bruin.squeezer.listitems.MusicFolderListFragment;
import stefan.de.bruin.squeezer.listitems.NewMusicListFragment;
import stefan.de.bruin.squeezer.listitems.PLaylistsListFragment;
import stefan.de.bruin.squeezer.listitems.RadioListFragment;
import stefan.de.bruin.squeezer.listitems.RandomMixListFragment;
import stefan.de.bruin.squeezer.listitems.SongsListFragment;
import stefan.de.bruin.squeezer.listitems.YearsListFragment;
import stefan.de.bruin.squeezer.service.IServiceConnectionCallback;
import stefan.de.bruin.squeezer.service.IServiceHandshakeCallback;
import stefan.de.bruin.squeezer.service.IServicePlayersCallback;
import stefan.de.bruin.squeezer.service.IServiceVolumeCallback;
import stefan.de.bruin.squeezer.service.ISqueezeService;
import stefan.de.bruin.squeezer.service.SqueezeService;

/**
 * dropdown array vullen in main activity
 * en meesturen
 * alles ombouwen naar setters en getters
 */

public class MainActivity extends BaseSqueezer {
    private final String TAG = "MainActivity";


    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    CustomDrawerAdapter adapter;

    List<DrawerItem> menuitems;
    List<PlayerModel> players;

    private VolumePanel mVolumePanel;
    /**
     * Volume control panel.
     */
    private boolean ignoreVolumeChange;

    boolean bCanMusicfolder = true;
    boolean bCanFavorites = true;
    boolean bCanMyApps = true;
    boolean bCanRandomplay = true;

//    public static final int ITEM_NEW_MUSIC = 1;
//    public static final int ITEM_ALBUMS = 2;
//    public static final int ITEM_ARTISTS = 3;
//    public static final int ITEM_SONGS = 4;
//    public static final int ITEM_GENRES = 5;
//    public static final int ITEM_YEARS = 6;
//    public static final int ITEM_PLAYLISTS = 7;
//    public static final int ITEM_INTERNET_RADIO = 8;
//    public static final int ITEM_RANDOM_MIX = 9;
//    public static final int ITEM_MUSIC_FOLDER = 10;
//    public static final int ITEM_FAVORITES = 11;
//    public static final int ITEM_MY_APPS = 12;
//    int _ITEM_RANDOM_MIX = 9;
//    int _ITEM_MUSIC_FOLDER = 10;
//    int _ITEM_FAVORITES = 11;
//    int _ITEM_MY_APPS = 12;

//    public static final int ITEM_SETTINGS = 14;
//    public static final int ITEM_PLAYERS = 15;
//    public static final int ITEM_ABOUT = 16;
//    int _ITEM_SETTINGS = 14;
//    int _ITEM_PLAYERS = 15;
//    int _ITEM_ABOUT = 16;

    private MenuItem menu_item_connect;
    private MenuItem menu_item_disconnect;
    private MenuItem menu_item_poweron;
    private MenuItem menu_item_poweroff;
    private MenuItem menu_item_players;
    private MenuItem menu_item_playlists;
    private MenuItem menu_item_search;
    private MenuItem menu_item_volume;

    private ProgressDialog connectingDialog = null;

    private final Handler uiThreadHandler = new NowPlayingFragment().getUiThreadHandler();

    private int secondsIn;
    private int secondsTotal;
    private final static int UPDATE_TIME = 1;

    private boolean mRegisteredCallbacks = false;

    public Navigationitems navigationitems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigationitems = new Navigationitems();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // Set up a server connection, if it is not present
        Log.d("ip adres", new Preferences(this).getServerAddress());
        if (new Preferences(this).getServerAddress() == null) {

            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);

//            Fragment fragment = new SettingsFragment();
//            Bundle args = new Bundle();
//            fragment.setArguments(args);
//            FragmentManager frgManager = getFragmentManager();
//            frgManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

//            mDrawerList.setItemChecked(position, true);
            setTitle("Settings");
            mDrawerLayout.closeDrawer(mDrawerList);
        }
        Squeezer.setContext(getApplicationContext());


        FillNavigation();

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, R.string.drawer_open,
                R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {

            if (menuitems.get(0).getSpinner() & menuitems.get(1).getTitle() != null) {
                SelectItem(2);
            } else if (menuitems.get(0).getTitle() != null) {
                SelectItem(1);
            } else {
                SelectItem(0);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Set up a server connection, if it is not present

        mVolumePanel = new VolumePanel(this);

        // Start it and have it run forever (until it shuts itself down).
        // This is required so swapping out the activity (and unbinding the
        // service connection in onDestroy) doesn't cause the service to be
        // killed due to zero refcount.  This is our signal that we want
        // it running in the background.
        this.startService(new Intent(this, SqueezeService.class));

        if (getService() != null) {
            //TODO specifiek voor nowplaying
            maybeRegisterCallbacks(getService());
//            updateUIFromServiceState();
        }

        if (new Preferences(this).isAutoConnect()) {
            this.registerReceiver(broadcastReceiver, new IntentFilter(
                    ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mVolumePanel.dismiss();

        Log.d(TAG, "onPause...");

        clearConnectingDialog();

        if (new Preferences(this).isAutoConnect()) {
            this.unregisterReceiver(broadcastReceiver);
        }

        if (mRegisteredCallbacks) {
            getService().cancelSubscriptions(this);
            mRegisteredCallbacks = false;
        }

        super.onPause();
    }

    @Override
    protected void registerCallback(@NonNull ISqueezeService service) {
        super.registerCallback(service);
        service.registerHandshakeCallback(mCallback);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * This is called when the service is first connected, and whenever the activity is resumed.
     */
    public void maybeRegisterCallbacks(@NonNull ISqueezeService service) {
        if (!mRegisteredCallbacks) {
            service.registerConnectionCallback(connectionCallback);
            service.registerHandshakeCallback(handshakeCallback);
            service.registerPlayersCallback(playersCallback);
            service.registerVolumeCallback(volumeCallback);
            mRegisteredCallbacks = true;
        }
    }

    /**
     * MainActivity
     */
    private final IServiceHandshakeCallback handshakeCallback = new IServiceHandshakeCallback() {
        @Override
        public void onHandshakeCompleted() {
            uiThreadHandler.post(new Runnable() {
                @Override
                public void run() {
                    updatePowerMenuItems(canPowerOn(), canPowerOff());
                }
            });
        }

        @Override
        public Object getClient() {
            return this;
//            return NowPlayingFragment.this;
        }
    };

    /**
     * MainActivity
     */
    private final IServicePlayersCallback playersCallback = new IServicePlayersCallback() {
        @Override
        public void onPlayersChanged(final List<PlayerModel> players, final PlayerModel activePlayer) {
            uiThreadHandler.post(new Runnable() {
                @Override
                public void run() {
                    //TODO function van maken ook voor de eertse keer van het invullen van de players in
//                    updatePlayerDropDown();
                    SetPlayers(players, activePlayer);
                }
            });
        }

        @Override
        public Object getClient() {
            return this;
//            return NowPlayingFragment.this;
        }
    };
    private final IServiceHandshakeCallback mCallback = new IServiceHandshakeCallback() {

        /**
         * Sets the menu after handshaking with the SqueezeServer has completed.
         * <p>
         * This is necessary because the service doesn't know whether the server
         * supports music folder browsing and random play ability until the
         * handshake completes, and the menu is adjusted depending on whether or
         * not those abilities exist.
         */
        @Override
        public void onHandshakeCompleted() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    FillNavigation();

                    // Show a tip about volume controls, if this is the first time this app
                    // has run. TODO: Add more robust and general 'tips' functionality.
                    PackageInfo pInfo;
                    try {
                        final SharedPreferences preferences = getSharedPreferences(Preferences.NAME,
                                0);

                        pInfo = getPackageManager().getPackageInfo(getPackageName(),
                                PackageManager.GET_META_DATA);
                        if (preferences.getLong("lastRunVersionCode", 0) == 0) {
//                            new TipsDialog().show(getSupportFragmentManager(), "TipsDialog");
//                            SharedPreferences.Editor editor = preferences.edit();
//                            editor.putLong("lastRunVersionCode", pInfo.versionCode);
//                            editor.commit();
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        // Nothing to do, don't crash.
                    }
                }
            });
        }

        @Override
        public Object getClient() {
            return MainActivity.this;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.squeezer, menu);

        menu_item_connect = menu.findItem(R.id.menu_item_connect);
        menu_item_disconnect = menu.findItem(R.id.menu_item_disconnect);
        menu_item_poweron = menu.findItem(R.id.menu_item_poweron);
        menu_item_poweroff = menu.findItem(R.id.menu_item_poweroff);
        menu_item_players = menu.findItem(R.id.menu_item_players);
        menu_item_playlists = menu.findItem(R.id.menu_item_playlist);
        menu_item_search = menu.findItem(R.id.menu_item_search);
        menu_item_volume = menu.findItem(R.id.menu_item_volume);
        return true;
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    //navigation
    private void FillNavigation(){

        // Initializing
        menuitems = new ArrayList<DrawerItem>();
        mTitle = mDrawerTitle = getTitle();

        if (getService() != null) {
            Log.d("debug can canFavorites", String.valueOf(getService().canFavorites()));
            Log.d("debug can canMusicfolder", String.valueOf(getService().canMusicfolder()));
            Log.d("debug can canMyApps", String.valueOf(getService().canMyApps()));
            Log.d("debug can canRandomplay", String.valueOf(getService().canRandomplay()));

            navigationitems.setmCanRandomplay(getService().canRandomplay());
            navigationitems.setmCanMusicfolder(getService().canMusicfolder());
            navigationitems.setmCanFavorites(getService().canFavorites());
            navigationitems.setmCanMyApps(getService().canMyApps());

        }else{
            Log.d("debug log", "FALSE");
        }

//        String[] arrayitems = getResources().getStringArray(R.array.navigation_item_title);
        navigationitems.setItems(getResources().getStringArray(R.array.navigation_item_title));
//        ArrayList<String> items = (ArrayList<String>) Arrays.asList(arrayitems);
//        int[] icons = getResources().getIntArray(R.array.navigation_item_images);

        // Add Drawer Item to dataList
        DrawerItem SpinnerMenu = new DrawerItem();
        SpinnerMenu.SetSpinnerItems(SetPlayers(null, null));
        menuitems.add(SpinnerMenu);

        for (int position = 1; position <= navigationitems.getEndnumber(); position++) {
            int arrayposition = position - 1;
            if (position == navigationitems.getMUSIC_FOLDER() && !navigationitems.ismCanMusicfolder()) {
                navigationitems.removeIcon(arrayposition);
                navigationitems.removeItem(arrayposition);
                continue;
            }

            if (position == navigationitems.getRANDOM_MIX() && !navigationitems.ismCanRandomplay()) {
                navigationitems.removeIcon(arrayposition);
                navigationitems.removeItem(arrayposition);
                continue;
            }

            if (position == navigationitems.getFAVORITES() && !navigationitems.ismCanFavorites()) {
                navigationitems.removeIcon(arrayposition);
                navigationitems.removeItem(arrayposition);
                continue;
            }

            if (position == navigationitems.getMY_APPS() && !navigationitems.ismCanMyApps()) {
                navigationitems.removeIcon(arrayposition);
                navigationitems.removeItem(arrayposition);
                continue;
            }

            DrawerItem item = new DrawerItem();
            item.setItemName(navigationitems.getItems(arrayposition));
            item.setImgResID(navigationitems.getIcons(arrayposition));
            menuitems.add(item);
        }

        menuitems.add(new DrawerItem("Main Options"));// adding a header to the list

        DrawerItem item = new DrawerItem();
        item.setItemName("Settings");
        item.setImgResID(R.drawable.ic_albums);
        menuitems.add(item);

        item = new DrawerItem();
        item.setItemName("Players");
        item.setImgResID(R.drawable.ic_albums);
        menuitems.add(item);

        item = new DrawerItem();
        item.setItemName("About");
        item.setImgResID(R.drawable.ic_albums);
        menuitems.add(item);

        adapter = new CustomDrawerAdapter(this, R.layout.custom_drawer_item, menuitems, getService());

        mDrawerList.setAdapter(adapter);
    }

    //@NonNull List<PlayerModel> players, @Nullable PlayerModel activePlayer
    public ArrayList<PlayerModel> SetPlayers(List<PlayerModel> players, PlayerModel activePlayer){
        ArrayList<PlayerModel> connectedPlayers = new ArrayList<PlayerModel>();
        for (PlayerModel player : players) {
            if (player.getConnected()) {
                connectedPlayers.add(player);
            }
        }
        return connectedPlayers;
    }

    public void SelectItem(int position) {
        Log.d("item position", String.valueOf(position));
        Log.d("item title", menuitems.get(position).getItemName());


        Fragment fragment = null;
        Bundle args = new Bundle();

        if((int) position == navigationitems.getNEW_MUSIC()){
            fragment = new NewMusicListFragment();
        }else if((int) position == navigationitems.getALBUMS()){
            fragment = new AlbumListFragment();
        }else if((int) position == navigationitems.getARTISTS()){
            fragment = new ArtistListFragment();
        }else if((int) position == navigationitems.getSONGS()){
            fragment = new SongsListFragment();
        }else if((int) position == navigationitems.getGENRES()){
            fragment = new GenresListFragment();
        }else if((int) position == navigationitems.getYEARS()){
            fragment = new YearsListFragment();
        }else if((int) position == navigationitems.getMUSIC_FOLDER() && navigationitems.ismCanMusicfolder()){
            fragment = new MusicFolderListFragment();
        }else if((int) position == navigationitems.getRANDOM_MIX() && navigationitems.ismCanRandomplay()){
            fragment = new RandomMixListFragment();
        }else if((int) position == navigationitems.getPLAYLISTS()){
            fragment = new PLaylistsListFragment();
        }else if((int) position == navigationitems.getINTERNET_RADIO()){
            fragment = new RadioListFragment();
        }else if((int) position == navigationitems.getFAVORITES() && navigationitems.ismCanFavorites()){
            fragment = new FavoritesListFragment();
        }else if((int) position == navigationitems.getMY_APPS() && navigationitems.ismCanMyApps()){
            fragment = new ApplicationListFragment();
        }else if((int) position == navigationitems.getPLAYERS()){
            fragment = new ApplicationListFragment();
        }else if((int) position == navigationitems.getSETTINGS()){
            fragment = new SettingsFragment();
        }else if((int) position == navigationitems.getABOUT()){
            fragment = new AboutFragment();
        }

        if(fragment != null) {
            fragment.setArguments(args);
            FragmentManager frgManager = getFragmentManager();
            frgManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .commit();

            mDrawerList.setItemChecked(position, true);
            setTitle(menuitems.get(position).getItemName());
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            SelectItem(position);
        }
    }
    // end navigation

    /**
     * VOLUME
     */
    private final IServiceVolumeCallback volumeCallback = new IServiceVolumeCallback() {
        @Override
        public void onVolumeChanged(final int newVolume, final PlayerModel player) {
            if (!ignoreVolumeChange) {
                mVolumePanel.postVolumeChanged(newVolume, player == null ? "" : player.getName());
            }
        }

        @Override
        public Object getClient() {
//            return NowPlayingFragment.this;
            return MainActivity.this;
        }

        @Override
        public boolean wantAllPlayers() {
            return false;
        }
    };

    public void setIgnoreVolumeChange(boolean ignoreVolumeChange) {
        this.ignoreVolumeChange = ignoreVolumeChange;
    }
    /**
     * END VOLUMNE
     */

    /**
     * start menu
     */
    /**
     * Sets the state of assorted option menu items based on whether or not there is a connection to
     * the server.
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean connected = isConnected();

        // These are all set at the same time, so one check is sufficient
        if (menu_item_connect != null) {
            menu_item_connect.setVisible(!connected);
            menu_item_disconnect.setVisible(connected);
            menu_item_players.setEnabled(connected);
            menu_item_playlists.setEnabled(connected);
            menu_item_search.setEnabled(connected);
            menu_item_volume.setEnabled(connected);
        }

        // Don't show the item to go to CurrentPlaylistActivity if in CurrentPlaylistActivity.
//        if (MainActivity instanceof CurrentPlaylistActivity && menu_item_playlists != null) {
//            menu_item_playlists.setVisible(false);
//        }

        updatePowerMenuItems(canPowerOn(), canPowerOff());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_search:
                this.onSearchRequested();
                return true;
            case R.id.menu_item_connect:
                onUserInitiatesConnect();
                return true;
            case R.id.menu_item_disconnect:
                //TODO change current content to disconnect fragment and change the navigation
                getService().disconnect();
                Fragment DisconnedtedFragment = null;
                DisconnedtedFragment = new DisconnedtedFragment();
                FragmentManager frgManager1 = getFragmentManager();
                frgManager1.beginTransaction().replace(R.id.content_frame, DisconnedtedFragment)
                        .commit();

//                DisconnectedActivity.show(this);
                return true;
            case R.id.menu_item_poweron:
                getService().powerOn();
                return true;
            case R.id.menu_item_poweroff:
                getService().powerOff();
                return true;
            case R.id.menu_item_playlist:
                //TODO change content to current playlist
                Fragment CurrentPlaylistActivity = null;
                CurrentPlaylistActivity = new CurrentPlaylistFragment();
                FragmentManager frgManager2 = getFragmentManager();
                frgManager2.beginTransaction().replace(R.id.content_frame, CurrentPlaylistActivity)
                        .commit();

//                CurrentPlaylistActivity.show(this);
                break;
            case R.id.menu_item_volume:
                // Show the volume dialog
                PlayerStateModel playerState = getPlayerState();
                PlayerModel player = getActivePlayer();

                if (playerState != null) {
                    mVolumePanel.postVolumeChanged(playerState.getCurrentVolume(),player == null ? "" : player.getName());
                }
                return true;
        }
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return false;
    }

    private PlayerStateModel getPlayerState() {
        if (getService() == null) {
            return null;
        }
        return getService().getPlayerState();
    }

    public void updatePowerMenuItems(boolean canPowerOn, boolean canPowerOff) {
        boolean connected = isConnected();

        if (menu_item_poweron != null) {
            if (canPowerOn && connected) {
                PlayerModel player = getActivePlayer();
                String playerName = player != null ? player.getName() : "";
                menu_item_poweron.setTitle(getString(R.string.menu_item_poweron, playerName));
                menu_item_poweron.setVisible(true);
            } else {
                menu_item_poweron.setVisible(false);
            }
        }

        if (menu_item_poweroff != null) {
            if (canPowerOff && connected) {
                PlayerModel player = getActivePlayer();
                String playerName = player != null ? player.getName() : "";
                menu_item_poweroff.setTitle(getString(R.string.menu_item_poweroff, playerName));
                menu_item_poweroff.setVisible(true);
            } else {
                menu_item_poweroff.setVisible(false);
            }
        }
    }
    /**
     * END MENU
     */

    /**
     * START CONNECTION TO CLI
     */

    /**
     * MainActivity
     * @param service
     */
    protected void onServiceConnected(@NonNull ISqueezeService service) {
        Log.v(TAG, "Service bound");
        setService(service);
//        mService = service;

        maybeRegisterCallbacks(service);
        uiThreadHandler.post(new Runnable() {
            @Override
            public void run() {

//                updateUIFromServiceState();
            }
        });

        // Assume they want to connect (unless manually disconnected).
        if (!isConnected() && !isManualDisconnect()) {
            startVisibleConnection();
        }
    }


    /**
     * MainActivity
     */
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (networkInfo.isConnected()) {
                Log.v(TAG, "Received WIFI connected broadcast");
                if (!isConnected()) {
                    // Requires a serviceStub. Else we'll do this on the service
                    // connection callback.
                    if (getService() != null && !isManualDisconnect()) {
                        Log.v(TAG, "Initiated connect on WIFI connected");
                        startVisibleConnection();
                    }
                }
            }
        }
    };

    private void onUserInitiatesConnect() {
        // Set up a server connection, if it is not present
        if (new Preferences(this).getServerAddress() == null) {
            //intent to server activity/fragment
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return;
        }

        if (getService() == null) {
            Log.e(TAG, "serviceStub is null.");
            return;
        }
        startVisibleConnection();
    }

    public void startVisibleConnection() {
        Log.v(TAG, "startVisibleConnection");
        if (getService() == null) {
            return;
        }

        uiThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                Preferences preferences = new Preferences(MainActivity.this);
                String ipPort = preferences.getServerAddress();
                if (ipPort == null) {
                    return;
                }

                // If we are configured to automatically connect on Wi-Fi availability
                // we will also give the user the opportunity to enable Wi-Fi
                if (preferences.isAutoConnect()) {
                    WifiManager wifiManager = (WifiManager) MainActivity.this
                            .getSystemService(Context.WIFI_SERVICE);
                    if (!wifiManager.isWifiEnabled()) {
//                        android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
//                        if (fragmentManager != null) {
//                            EnableWifiDialog.show(getFragmentManager());
//                        } else {
//                            Log.i(getTag(), "fragment manager is null so we can't show EnableWifiDialog");
//                        }
//                        return;
                        // When a Wi-Fi connection is made this method will be called again by the
                        // broadcastReceiver
                    }
                }

                if (isConnectInProgress()) {
                    Log.v(TAG, "Connection is already in progress, connecting aborted");
                    return;
                }
                try {
                    connectingDialog = ProgressDialog.show(MainActivity.this,
                            getText(R.string.connecting_text),
                            getString(R.string.connecting_to_text, preferences.getServerName()), true, false);
                    Log.v(TAG, "startConnect, ipPort: " + ipPort);
                    getService().startConnect(ipPort, preferences.getUserName("test"),
                            preferences.getPassword("test1"));
                } catch (IllegalStateException e) {
                    Log.i(TAG, "ProgressDialog.show() was not allowed, connecting aborted: " + e);
                    connectingDialog = null;
                }
            }
        });
    }

    private boolean isConnectInProgress() {
        if (getService() == null) {
            return false;
        }
        return getService().isConnectInProgress();
    }

    private boolean canPowerOn() {
        if (getService() == null) {
            return false;
        }
        return getService().canPowerOn();
    }

    private boolean canPowerOff() {
        if (getService() == null) {
            return false;
        }
        return getService().canPowerOff();
    }

    private PlayerModel getActivePlayer() {
        if (getService() == null) {
            return null;
        }
        return getService().getActivePlayer();
    }

    private void clearConnectingDialog() {
        if (connectingDialog != null && connectingDialog.isShowing()) {
            connectingDialog.dismiss();
        }
        connectingDialog = null;
    }


    /**
     * MainActivity
     */
    private final IServiceConnectionCallback connectionCallback = new IServiceConnectionCallback() {
        @Override
        public void onConnectionChanged(final boolean isConnected, final boolean postConnect, final boolean loginFailed) {
            Log.v(TAG, "Connected == " + isConnected + " (postConnect==" + postConnect + ")");
            uiThreadHandler.post(new Runnable() {
                @Override
                public void run() {
                    //TODO staat nu in nowplaying en vult de velden in als start van de onnectie met die waarden.
//                    setConnected(isConnected, postConnect, loginFailed);
                }
            });
        }

        @Override
        public Object getClient() {
            //TODO Werken maken
            return this;
//            return NowPlayingFragment.this;
        }
    };

    /**
     * MainActivity
     * Has the user manually disconnected from the server?
     *
     * @return true if they have, false otherwise.
     */
    private boolean isManualDisconnect() {
        //TODO werkend maken regel onder return true
        return true;
//        return getActivity() instanceof DisconnectedActivity;
    }


    /**
     * END START CONNECTION TO CLI
     */
}
