package stefan.de.bruin.squeezer;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Stefan on 29-1-2015.
 */
public class Navigationitems {

    private int NEW_MUSIC = 1;
    private int ALBUMS = 3;
    private int ARTISTS = 2;
    private int SONGS = 4;
    private int GENRES = 5;
    private int YEARS = 6;
    private int PLAYLISTS = 7;
    private int INTERNET_RADIO = 8;
    private int RANDOM_MIX = 9;
    private int MUSIC_FOLDER = 10;
    private int FAVORITES = 11;
    private int MY_APPS = 12;

    private int PLAYERS = 14;
    private int SETTINGS = 15;
    private int ABOUT = 16;

    private int endnumber = 16;

    private boolean mCanMusicfolder = false;
    private boolean mCanRandomplay = false;
    private boolean mCanFavorites = false;
    private boolean mCanMyApps = false;

    private ArrayList<String> items = null;
    private ArrayList<Integer> icons;

    public void Navigationitems(){
        icons.add(R.drawable.ic_new_music);
        icons.add(R.drawable.ic_albums);
        icons.add(R.drawable.ic_artists);
        icons.add(R.drawable.ic_songs);
        icons.add(R.drawable.ic_genres);
        icons.add(R.drawable.ic_years);
        icons.add(R.drawable.ic_internet_radio);
        icons.add(R.drawable.ic_music_folder);
        icons.add(R.drawable.ic_favorites);
        icons.add(R.drawable.ic_my_apps);

    }

    public boolean ismCanFavorites() {
        return mCanFavorites;
    }

    public boolean ismCanMusicfolder() {
        return mCanMusicfolder;
    }

    public boolean ismCanMyApps() {
        return mCanMyApps;
    }

    public boolean ismCanRandomplay() {
        return mCanRandomplay;
    }

    public void setmCanMusicfolder(boolean mCanMusicfolder) {
        this.mCanMusicfolder = mCanMusicfolder;
        if(mCanMusicfolder == false){
            this.FAVORITES--;
            endbumbermin();
            this.MY_APPS--;
            endbumbermin();
            this.RANDOM_MIX--;
            endbumbermin();
            setotheritems();
        }
    }

    public void setmCanRandomplay(boolean mCanRandomplay) {
        this.mCanRandomplay = mCanRandomplay;
        if(mCanRandomplay == false){
            this.FAVORITES--;
            endbumbermin();
            this.MY_APPS--;
            endbumbermin();
            setotheritems();
        }
    }

    public void setmCanFavorites(boolean mCanFavorites) {
        this.mCanFavorites = mCanFavorites;
        if(mCanFavorites == false){
            this.MY_APPS--;
            endbumbermin();
            setotheritems();
        }
    }

    public void setmCanMyApps(boolean mCanMyApps) {
        this.mCanMyApps = mCanMyApps;
        if(mCanMyApps == false){
            setotheritems();
        }
    }

    public int getALBUMS(){
        return ALBUMS;
    }

    public int getARTISTS(){
        return ARTISTS;
    }

    public int getSONGS(){
        return SONGS;
    }

    public int getGENRES(){
        return GENRES;
    }

    public int getYEARS(){
        return YEARS;
    }

    public int getNEW_MUSIC(){
        return NEW_MUSIC;
    }

    public int getMUSIC_FOLDER(){
        return MUSIC_FOLDER;
    }

    public int getRANDOM_MIX(){
        return RANDOM_MIX;
    }

    public int getPLAYLISTS(){
        return PLAYLISTS;
    }

    public int getINTERNET_RADIO(){
        return INTERNET_RADIO;
    }

    public int getFAVORITES(){
        return FAVORITES;
    }

    public int getMY_APPS(){
        return MY_APPS;
    }

    public int getPLAYERS() {
        return PLAYERS;
    }

    public int getSETTINGS() {
        return SETTINGS;
    }

    public int getABOUT() {
        return ABOUT;
    }

    public int getEndnumber() {
        return endnumber;
    }

    public ArrayList<Integer> getIcons() {
        return icons;
    }

    public ArrayList<String> getItems() {
        return items;
    }

    public Integer getIcons(int id) {
        return icons.get(id);
    }

    public String getItems(int id){
        return items.get(id);
    }

    public void setALBUMS(int ALBUMS) {
        this.ALBUMS = ALBUMS;
    }

    public void setARTISTS(int ARTISTS) {
        this.ARTISTS = ARTISTS;
    }

    public void setFAVORITES(int FAVORITES) {
        this.FAVORITES = FAVORITES;
    }

    public void setGENRES(int GENRES) {
        this.GENRES = GENRES;
    }

    public void setINTERNET_RADIO(int INTERNET_RADIO) {
        this.INTERNET_RADIO = INTERNET_RADIO;
    }

    public void setMUSIC_FOLDER(int MUSIC_FOLDER) {
        this.MUSIC_FOLDER = MUSIC_FOLDER;
    }

    public void setMY_APPS(int MY_APPS) {
        this.MY_APPS = MY_APPS;
    }

    public void setNEW_MUSIC(int NEW_MUSIC) {
        this.NEW_MUSIC = NEW_MUSIC;
    }

    public void setPLAYLISTS(int PLAYLISTS) {
        this.PLAYLISTS = PLAYLISTS;
    }

    public void setRANDOM_MIX(int RANDOM_MIX) {
        this.RANDOM_MIX = RANDOM_MIX;
    }

    public void setSONGS(int SONGS) {
        this.SONGS = SONGS;
    }

    public void setYEARS(int YEARS) {
        this.YEARS = YEARS;
    }

    public void setPLAYERS(int PLAYERS) {
        this.PLAYERS = PLAYERS;
    }

    public void setSETTINGS(int SETTINGS) {
        this.SETTINGS = SETTINGS;
    }

    public void setEndnumber(int endnumber) {
        this.endnumber = endnumber;
    }

    public void setABOUT(int ABOUT) {
        this.ABOUT = ABOUT;
    }

    public void setItems(ArrayList<String> items) {
        this.items = items;
    }

    public void setItems(String[] items){
        this.items = (ArrayList<String>) Arrays.asList(items);
    }

    public void removeItem(int id){
        this.icons.remove(id);
    }

    public void removeIcon(int id){
        this.items.remove(id);
    }

    private void setotheritems(){
        this.PLAYERS--;
        endbumbermin();
        this.SETTINGS--;
        endbumbermin();
        this.ABOUT--;
        endbumbermin();
    }

    private void endbumbermin(){
        endnumber--;
    }
}
