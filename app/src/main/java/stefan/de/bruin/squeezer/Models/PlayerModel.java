/*
 * Copyright (c) 2011 Kurt Aaholst <kaaholst@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stefan.de.bruin.squeezer.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Map;

import stefan.de.bruin.squeezer.Util;
import stefan.de.bruin.squeezer.framework.Item;


public class PlayerModel extends Item {

    private String mName;

    private final String mIp;

    private final String mModel;

    private final boolean mCanPowerOff;

    /** Is the player connected? */
    private boolean mConnected;

    public PlayerModel(Map<String, String> record) {
        setId(record.get("playerid"));
        mIp = record.get("ip");
        mName = record.get("name");
        mModel = record.get("model");
        mCanPowerOff = Util.parseDecimalIntOrZero(record.get("canpoweroff")) == 1;
        mConnected = Util.parseDecimalIntOrZero(record.get("connected")) == 1;
    }

    private PlayerModel(Parcel source) {
        setId(source.readString());
        mIp = source.readString();
        mName = source.readString();
        mModel = source.readString();
        mCanPowerOff = (source.readByte() == 1);
        mConnected = (source.readByte() == 1);
    }

    @Override
    public String getName() {
        return mName;
    }

    public PlayerModel setName(String name) {
        this.mName = name;
        return this;
    }

    public String getIp() {
        return mIp;
    }

    public String getModel() {
        return mModel;
    }

    public boolean isCanpoweroff() {
        return mCanPowerOff;
    }

    public void setConnected(boolean connected) {
        mConnected = connected;
    }

    public boolean getConnected() {
        return mConnected;
    }

    public static final Parcelable.Creator<PlayerModel> CREATOR = new Parcelable.Creator<PlayerModel>() {
        public PlayerModel[] newArray(int size) {
            return new PlayerModel[size];
        }

        public PlayerModel createFromParcel(Parcel source) {
            return new PlayerModel(source);
        }
    };

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(mIp);
        dest.writeString(mName);
        dest.writeString(mModel);
        dest.writeByte(mCanPowerOff ? (byte) 1 : (byte) 0);
        dest.writeByte(mConnected ? (byte) 1 : (byte) 0);
    }

    @Override
    public String toString() {
        return "id=" + getId() + ", name=" + mName + ", model=" + mModel + ", canpoweroff="
                + mCanPowerOff + ", ip=" + mIp + ", connected=" + mConnected;
    }
}
