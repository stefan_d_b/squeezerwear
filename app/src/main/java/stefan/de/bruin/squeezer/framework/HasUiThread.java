package stefan.de.bruin.squeezer.framework;

import android.os.Handler;

public interface HasUiThread {

    Handler getUIThreadHandler();
}
