package stefan.de.bruin.squeezer;

import java.util.ArrayList;

import stefan.de.bruin.squeezer.Models.PlayerModel;

/**
 * Created by Stefan on 13-1-2015.
 */
public class DrawerItem {

    String ItemName;
    int imgResID;
    String title;
    boolean isSpinner = false;
    ArrayList<PlayerModel> players;
    PlayerModel activeplayer;

    public DrawerItem(String itemName, int imgResID) {
        ItemName = itemName;
        this.imgResID = imgResID;
    }

    public DrawerItem(boolean isSpinner) {
        this(null, 0);
        this.isSpinner = isSpinner;
    }

    public DrawerItem(){

    }

    public DrawerItem(String title) {
        this(null, 0);
        this.title = title;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public int getImgResID() {
        return imgResID;
    }

    public void setImgResID(int imgResID) {
        this.imgResID = imgResID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean getSpinner() {
        return isSpinner;
    }

    public void SetSpinnerItems(ArrayList<PlayerModel> p){
        this.isSpinner = true;
        players = p;
    }

    public ArrayList<PlayerModel> getSpinnerItems(){
        return players;
    }

    public void setActivePlayer(PlayerModel p){
        activeplayer = p;
    }

    public int getActivePlayerID(){
        return 1;
    }

    public PlayerModel getActivePlayer(){
        return activeplayer;
    }
}
