package stefan.de.bruin.squeezer.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Map;
import stefan.de.bruin.squeezer.framework.PlaylistItem;

/**
 * Created by Stefan on 16-1-2015.
 */
public class ArtistModel extends PlaylistItem {

    private int image;
    private String name;
    private String artist;
    private int year;

    @Override
    public String getPlaylistTag() {
        return "album_id";
    }

    @Override
    public String getFilterTag() {
        return "album_id";
    }

    public String getName() {
        return this.name;
    }

    public String getArtist() {
        return artist;
    }

    public int getYear() {
        return year;
    }

    public ArtistModel setName(String title) {
        this.name = title;
        return this;
    }

    public void setYear(int y) {
        this.year = y;
    }

    public void setArtist(String artist) {
        artist = artist;
    }

    public ArtistModel(String albumId, String album) {
        setId(albumId);
        setName(album);
    }

    public ArtistModel(Map<String, String> record) {
        setId(record.containsKey("contributor_id") ? record.get("contributor_id")
                : record.get("id"));
        name = record.containsKey("contributor") ? record.get("contributor") : record.get("artist");
    }

    private ArtistModel(Parcel source) {
        setId(source.readString());
        name = source.readString();
    }

    public ArtistModel(){

    }

    public static final Parcelable.Creator<ArtistModel> CREATOR = new Parcelable.Creator<ArtistModel>() {
        public ArtistModel[] newArray(int size) {
            return new ArtistModel[size];
        }

        public ArtistModel createFromParcel(Parcel source) {
            return new ArtistModel(source);
        }
    };

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(name);
    }

    @Override
    public String toString() {
        return "id=" + getId() + ", name=" + name;
    }
}
