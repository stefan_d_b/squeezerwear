package stefan.de.bruin.squeezer.itemlist.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.text.InputType;

import stefan.de.bruin.squeezer.R;
//import stefan.de.bruin.squeezer.framework.BaseActivity;
import stefan.de.bruin.squeezer.framework.BaseSqueezer;
import stefan.de.bruin.squeezer.service.ISqueezeService;

public class PlaylistSaveDialog extends BaseEditTextDialog {

    private BaseSqueezer activity;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        Bundle args = getArguments();
        String name = args.getString("name");

        activity = (BaseSqueezer) getActivity();
        dialog.setTitle(R.string.save_playlist_title);
        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        editText.setHint(R.string.save_playlist_hint);
        if (name != null && name.length() > 0) {
            editText.setText(name);
        }

        return dialog;
    }

    @Override
    protected boolean commit(String name) {
        ISqueezeService service = activity.getService();
        if (service == null) {
            return false;
        }

        service.playlistSave(name);
        return true;
    }

    public static void addTo(BaseSqueezer activity, String name) {
        PlaylistSaveDialog dialog = new PlaylistSaveDialog();
        Bundle args = new Bundle();
        args.putString("name", name);
        dialog.setArguments(args);
        dialog.show(activity.getSupportFragmentManager(), "SaveDialog");
    }
}
