package stefan.de.bruin.squeezer.Models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Map;
import com.google.common.base.Objects;
import com.google.common.base.Strings;

import stefan.de.bruin.squeezer.Util;
import stefan.de.bruin.squeezer.framework.ArtworkItem;
import stefan.de.bruin.squeezer.service.ISqueezeService;

/**
 * Created by Stefan on 17-1-2015.
 */
public class SongModel extends ArtworkItem {

    @NonNull private final String mArtist;
    @NonNull private AlbumModel mAlbum;
    @NonNull private final String mAlbumName;
    private final boolean mCompilation;
    private final int mDuration;
    private final int mYear;

    @NonNull private final String mArtistId;

    @NonNull private final String mUrl;
    private boolean mRemote;
    @NonNull private String mArtworkUrl;
    @NonNull private final String mAlbumId;
    private final int mTrackNum;




    @Override
    public String getPlaylistTag() {
        return "track_id";
    }

    @Override
    public String getFilterTag() {
        return "track_id";
    }

    /** The "track" or "title" value from the server. */
    @NonNull
    private final String mName;

    @Override
    @NonNull
    public String getName() {
        return mName;
    }

    @NonNull
    public String getArtist() {
        return mArtist;
    }

    @NonNull
    public AlbumModel getAlbum() {
        return mAlbum;
    }

    @NonNull
    public String getAlbumName() {
        return mAlbumName;
    }

    public boolean getCompilation() {
        return mCompilation;
    }

    public int getDuration() {
        return mDuration;
    }

    public int getYear() {
        return mYear;
    }

    @NonNull
    public String getArtistId() {
        return mArtistId;
    }

    @NonNull
    public String getAlbumId() {
        return mAlbumId;
    }

    public boolean isRemote() {
        return mRemote;
    }

    public int getTrackNum() {
        return mTrackNum;
    }

    @NonNull
    public String getUrl() {
        return mUrl;
    }



    /**
     * @return Whether the song has artwork associated with it.
     */
    public boolean hasArtwork() {
        if (!mRemote) {
            return getArtwork_track_id() != null;
        } else {
            return ! "".equals(mArtworkUrl);
        }
    }

    @NonNull
    public String getArtworkUrl() {
        return mArtworkUrl;
    }

    @Nullable
    public String getArtworkUrl(ISqueezeService service) {
        if (getArtwork_track_id() != null) {
            if (service == null) {
                return null;
            }
            return service.getAlbumArtUrl(getArtwork_track_id());
        }
        return getArtworkUrl();
    }

    public SongModel(Map<String, String> record) {
        if (getId() == null) {
            setId(record.get("track_id"));
        }
        if (getId() == null) {
            setId(record.get("id"));
        }

        mName = record.containsKey("track") ? Strings.nullToEmpty(record.get("track"))
                : Strings.nullToEmpty(record.get("title"));

        mArtist = Strings.nullToEmpty(record.get("artist"));
        mAlbumName = Strings.nullToEmpty(record.get("album"));
        mCompilation = Util.parseDecimalIntOrZero(record.get("compilation")) == 1;
        mDuration = Util.parseDecimalIntOrZero(record.get("duration"));
        mYear = Util.parseDecimalIntOrZero(record.get("year"));
        mArtistId = Strings.nullToEmpty(record.get("artist_id"));
        mAlbumId = Strings.nullToEmpty(record.get("album_id"));
        mRemote = Util.parseDecimalIntOrZero(record.get("remote")) != 0;
        mTrackNum = Util.parseDecimalInt(record.get("tracknum"), 1);
        mArtworkUrl = Strings.nullToEmpty(record.get("artwork_url"));
        mUrl = Strings.nullToEmpty(record.get("url"));

        // Work around a (possible) bug in the Squeezeserver.
        //
        // I've seen tracks where the "coverart" tag comes back positive (1)
        // but there's no "artwork_track_id" tag. If that happens, use this
        // song's ID as the artwork_track_id.
        String artworkTrackId = record.get("artwork_track_id");
        if (artworkTrackId != null) {
            setArtwork_track_id(artworkTrackId);
        } else {
            // If there's no cover art then the server doesn't respond
            // "coverart:0" or something useful like that, it just doesn't
            // include a response.  Hence these shenanigans.
            String coverArt = record.get("coverart");
            if ("1".equals(coverArt)) {
                setArtwork_track_id(getId());
            }
        }

        AlbumModel album = new AlbumModel(mAlbumId, mAlbumName);
        album.setArtist(mCompilation ? "Various" : mArtist);
        album.setArtwork_track_id(artworkTrackId);
        album.setYear(mYear);
        mAlbum = album;
    }

    public static final Parcelable.Creator<SongModel> CREATOR = new Parcelable.Creator<SongModel>() {
        public SongModel[] newArray(int size) {
            return new SongModel[size];
        }

        public SongModel createFromParcel(Parcel source) {
            return new SongModel(source);
        }
    };

    private SongModel(Parcel source) {
        setId(source.readString());
        mName = source.readString();
        mArtist = source.readString();
        mAlbumName = source.readString();
        mCompilation = source.readInt() == 1;
        mDuration = source.readInt();
        mYear = source.readInt();
        mArtistId = source.readString();
        mAlbumId = source.readString();
        setArtwork_track_id(source.readString());
        mTrackNum = source.readInt();
        mUrl = source.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(mName);
        dest.writeString(mArtist);
        dest.writeString(mAlbumName);
        dest.writeInt(mCompilation ? 1 : 0);
        dest.writeInt(mDuration);
        dest.writeInt(mYear);
        dest.writeString(mArtistId);
        dest.writeString(mAlbumId);
        dest.writeString(getArtwork_track_id());
        dest.writeInt(mTrackNum);
        dest.writeString(mUrl);
    }

    @Override
    public String toString() {
        return "id=" + getId() + ", mName=" + mName + ", mArtist=" + mArtist + ", year=" + mYear;
    }

    /**
     * Extend the equality test by looking at additional track information.
     * <p/>
     * This is to deal with songs from remote streams where the stream might provide a single
     * song ID for multiple consecutive songs in the stream.
     *
     * @param o The object to test.
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) {
            return false;
        }

        // super.equals() has already checked that o is not null and is of the same class.
        SongModel s = (SongModel)o;

        if (! s.getName().equals(mName)) {
            return false;
        }

        if (! s.getAlbumName().equals(mAlbumName)) {
            return false;
        }

        if (! s.getArtist().equals(mArtist)) {
            return false;
        }

        if (! s.getArtworkUrl().equals(mArtworkUrl)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId(), mName, mAlbumName, mArtist, mArtworkUrl);
    }
}
