package stefan.de.bruin.squeezer;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan on 13-1-2015.
 */

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import stefan.de.bruin.squeezer.Models.PlayerModel;

public class CustomSpinnerAdapter extends ArrayAdapter<PlayerModel>{

    public CustomSpinnerAdapter(Context context, int layoutResourceID, int textViewResourceId, List<PlayerModel> spinnerDataList) {
        super(context, layoutResourceID, textViewResourceId, spinnerDataList);

        this.context=context;
        this.layoutResID=layoutResourceID;
        this.spinnerData=spinnerDataList;
    }

    Context context;
    int layoutResID;
    List<PlayerModel> spinnerData;

    public CustomSpinnerAdapter(Context context, int layoutResourceID, ArrayList<PlayerModel> spinnerDataList) {
        super(context, layoutResourceID, spinnerDataList);

        this.context=context;
        this.layoutResID=layoutResourceID;
        this.spinnerData=spinnerDataList;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        View row=convertView;
        SpinnerHolder holder;

        if(row==null){
            LayoutInflater inflater=((Activity)context).getLayoutInflater();
            row=inflater.inflate(layoutResID, parent, false);
            holder=new SpinnerHolder();

//            holder.userImage=(ImageView)row.findViewById(R.id.left_pic);
            holder.name = (TextView)row.findViewById(R.id.text_main_name);
//            holder.email=(TextView)row.findViewById(R.id.sub_text_email);

            row.setTag(holder);
        }else{
            holder=(SpinnerHolder)row.getTag();
        }

        PlayerModel spinnerItem = spinnerData.get(position);

        holder.name.setText(spinnerItem.getName());

        return row;
    }

    private static class SpinnerHolder{
        ImageView userImage;
        TextView  name,email;
    }

}
