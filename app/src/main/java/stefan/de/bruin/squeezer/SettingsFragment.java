package stefan.de.bruin.squeezer;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.view.View;


/**
 * Created by Stefan on 18-1-2015.
 */
public class SettingsFragment extends PreferenceFragment{
    private static final boolean ALWAYS_SIMPLE_PREFS = false;
    public View _container;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }
}
