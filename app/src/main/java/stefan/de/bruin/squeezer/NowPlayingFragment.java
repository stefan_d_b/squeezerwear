/*
 * Copyright (c) 2012 Google Inc.  All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stefan.de.bruin.squeezer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;

import stefan.de.bruin.squeezer.Models.ArtistModel;
import stefan.de.bruin.squeezer.Models.PlayerModel;
import stefan.de.bruin.squeezer.Models.PlayerStateModel;
import stefan.de.bruin.squeezer.Models.PlayerStateModel.PlayStatus;
import stefan.de.bruin.squeezer.Models.PlayerStateModel.RepeatStatus;
import stefan.de.bruin.squeezer.Models.PlayerStateModel.ShuffleStatus;
import stefan.de.bruin.squeezer.Models.SongModel;
import stefan.de.bruin.squeezer.framework.BaseSqueezer;
import stefan.de.bruin.squeezer.framework.HasUiThread;
//import stefan.de.bruin.squeezer.itemlist.AlbumListActivity;
//import stefan.de.bruin.squeezer.itemlist.SongListActivity;
import stefan.de.bruin.squeezer.listitems.AlbumListFragment;
import stefan.de.bruin.squeezer.listitems.SongsListFragment;
import stefan.de.bruin.squeezer.service.BluetoothService;
import stefan.de.bruin.squeezer.service.IServiceCallback;
import stefan.de.bruin.squeezer.service.IServiceConnectionCallback;
import stefan.de.bruin.squeezer.service.IServiceHandshakeCallback;
import stefan.de.bruin.squeezer.service.IServiceMusicChangedCallback;
import stefan.de.bruin.squeezer.service.IServicePlayersCallback;
import stefan.de.bruin.squeezer.service.ISqueezeService;
import stefan.de.bruin.squeezer.service.SqueezeService;
import stefan.de.bruin.squeezer.util.AsyncTask;
import stefan.de.bruin.squeezer.util.ImageCache.ImageCacheParams;
import stefan.de.bruin.squeezer.util.ImageFetcher;

//import android.support.v7.app.ActionBar;
//import stefan.de.bruin.squeezer.Dialogs.AboutDialog;

public class NowPlayingFragment extends Fragment implements
        HasUiThread, View.OnCreateContextMenuListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private final String TAG = "NowPlayingFragment";

    private BaseSqueezer mActivity;

    @Nullable private ISqueezeService mService = null;

    private TextView albumText;

    private TextView artistText;

    private TextView trackText;

    ImageView btnContextMenu;

    private TextView currentTime;

    private TextView totalTime;

    private ImageButton playPauseButton;

    private ImageButton nextButton;

    private ImageButton prevButton;

    private ImageButton shuffleButton;

    private ImageButton repeatButton;

    private ImageView albumArt;

    /** In full-screen mode, shows the current progress through the track. */
    private SeekBar seekBar;

    /** In mini-mode, shows the current progress through the track. */
    private ProgressBar mProgressBar;

    // Updating the seekbar
    private boolean updateSeekBar = true;

    private int secondsIn;

    private int secondsTotal;

    private final static int UPDATE_TIME = 1;

    /**
     * ImageFetcher for album cover art
     */
    private ImageFetcher mImageFetcher;

    /**
     * ImageCache parameters for the album art.
     */
    private ImageCacheParams mImageCacheParams;

    private final Handler uiThreadHandler = new UiThreadHandler(this);

    private GoogleApiClient googleClient;

    private ProgressDialog connectingDialog = null;

    private boolean mFullHeightLayout;

    /**
     * Keep track of whether callbacks have been registered
     */
    private boolean mRegisteredCallbacks = false;

    public Handler getUiThreadHandler(){
        return uiThreadHandler;
    }

    /**
     * NowPlayingragment
     */
    public final static class UiThreadHandler extends Handler {

        final WeakReference<NowPlayingFragment> mFragment;

        public UiThreadHandler(NowPlayingFragment fragment) {
            mFragment = new WeakReference<NowPlayingFragment>(fragment);
        }

        // Normally I'm lazy and just post Runnables to the uiThreadHandler
        // but time updating is special enough (it happens every second) to
        // take care not to allocate so much memory which forces Dalvik to GC
        // all the time.
        @Override
        public void handleMessage(Message message) {
            if (message.what == UPDATE_TIME) {
                mFragment.get().updateTimeDisplayTo(mFragment.get().secondsIn,
                        mFragment.get().secondsTotal);
            }
        }
    }

    /**
     * TODO TWIJFEL
     */
    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            Log.v(TAG, "ServiceConnection.onServiceConnected()");
//            NowPlayingFragment.this.onServiceConnected((ISqueezeService) binder);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };

    /**
     * Now PlayingFragment
     */
    /**
     * Called before onAttach. Pull out the layout spec to figure out which layout to use later.
     */
    @Override
    public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState) {
        super.onInflate(activity, attrs, savedInstanceState);

        int layout_height = attrs.getAttributeUnsignedIntValue(
                "http://schemas.android.com/apk/res/android",
                "layout_height", 0);

        mFullHeightLayout = (layout_height == ViewGroup.LayoutParams.FILL_PARENT);
    }

    /**
     * NowPlayingFragment
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (BaseSqueezer) activity;
    }

    /**
     * NowPlayingFragment
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        googleClient = new GoogleApiClient.Builder(mActivity)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        // Set up a server connection, if it is not present
//        if (new Preferences(mActivity).getServerAddress() == null) {
////            SettingsActivity.show(mActivity);
//            //intent naar settings activity/fragment
//            Log.d(TAG, "geen server adres ingevuld");
//            return;
//        }

        mActivity.bindService(new Intent(mActivity, SqueezeService.class), serviceConnection, Context.BIND_AUTO_CREATE);
        Log.d(TAG, "did bindService; serviceStub = " + mService);
    }

    /**
     * NowPlayingFragment
     */
    @Override
    public void onStop() {
        if (null != googleClient && googleClient.isConnected()) {
            googleClient.disconnect();
        }
        super.onStop();
    }

    /**
     * NowPlayingFragment
     */
    @Override
    public void onStart() {
        if (null != googleClient && !googleClient.isConnected()) {
            googleClient.connect();
        }
        super.onStop();
    }

    /**
     * NowPlayingFragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v;

        if (mFullHeightLayout) {
            v = inflater.inflate(R.layout.now_playing_full, container, false);

            artistText = (TextView) v.findViewById(R.id.artistname);
            nextButton = (ImageButton) v.findViewById(R.id.next);
            prevButton = (ImageButton) v.findViewById(R.id.prev);
            shuffleButton = (ImageButton) v.findViewById(R.id.shuffle);
            repeatButton = (ImageButton) v.findViewById(R.id.repeat);
            currentTime = (TextView) v.findViewById(R.id.currenttime);
            totalTime = (TextView) v.findViewById(R.id.totaltime);
            seekBar = (SeekBar) v.findViewById(R.id.seekbar);

            btnContextMenu = (ImageView) v.findViewById(R.id.context_menu);
            btnContextMenu.setOnCreateContextMenuListener(this);
            btnContextMenu.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.showContextMenu();
                }
            });

            // Calculate the size of the album art to display, which will be the shorter
            // of the device's two dimensions.
            Display display = mActivity.getWindowManager().getDefaultDisplay();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            display.getMetrics(displayMetrics);
            mImageFetcher = new ImageFetcher(mActivity,
                    Math.min(displayMetrics.heightPixels, displayMetrics.widthPixels));
        } else {
            v = inflater.inflate(R.layout.now_playing_mini, container, false);

            mProgressBar = (ProgressBar) v.findViewById(R.id.progressbar);

            // Get an ImageFetcher to scale artwork to the size of the icon view.
            Resources resources = getResources();
            int iconSize = (Math.max(
                    resources.getDimensionPixelSize(R.dimen.album_art_icon_height),
                    resources.getDimensionPixelSize(R.dimen.album_art_icon_width)));
            mImageFetcher = new ImageFetcher(mActivity, iconSize);
        }

        // TODO: Clean this up.  I think a better approach is to create the cache
        // in the activity that hosts the fragment, and make the cache available to
        // the fragment (or, make the cache a singleton across the whole app).
        mImageFetcher.setLoadingImage(R.drawable.icon_pending_artwork);
        mImageCacheParams = new ImageCacheParams(mActivity, "artwork");
        mImageCacheParams.setMemCacheSizePercent(mActivity, 0.12f);

        albumArt = (ImageView) v.findViewById(R.id.album);
        trackText = (TextView) v.findViewById(R.id.trackname);
        albumText = (TextView) v.findViewById(R.id.albumname);
        playPauseButton = (ImageButton) v.findViewById(R.id.pause);

        // Marquee effect on TextViews only works if they're focused.
        trackText.requestFocus();

        playPauseButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (mService == null) {
                    return;
                }
                if (isConnected()) {
                    Log.v(TAG, "Pause...");
                    mService.togglePausePlay();
                } else {
                    // When we're not connected, the play/pause
                    // button turns into a green connect button.
                    // TODO controleren goede plaats onUserInitiatesConnect
//                    onUserInitiatesConnect();
                }
            }
        });

        if (mFullHeightLayout) {
            /*
             * TODO: Simplify these following the notes at
             * http://developer.android.com/resources/articles/ui-1.6.html.
             * Maybe. because the TextView resources don't support the
             * android:onClick attribute.
             */
            nextButton.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (mService == null) {
                        return;
                    }
                    mService.nextTrack();
                }
            });

            prevButton.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (mService == null) {
                        return;
                    }
                    mService.previousTrack();
                }
            });

            shuffleButton.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (mService == null) {
                        return;
                    }
                    mService.toggleShuffle();
                }
            });

            repeatButton.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (mService == null) {
                        return;
                    }
                    mService.toggleRepeat();
                }
            });

            seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                SongModel seekingSong;

                // Update the time indicator to reflect the dragged thumb
                // position.
                public void onProgressChanged(SeekBar s, int progress, boolean fromUser) {
                    if (fromUser) {
                        currentTime.setText(Util.formatElapsedTime(progress));
                    }
                }

                // Disable updates when user drags the thumb.
                public void onStartTrackingTouch(SeekBar s) {
                    seekingSong = getCurrentSong();
                    updateSeekBar = false;
                }

                // Re-enable updates. If the current song is the same as when
                // we started seeking then jump to the new point in the track,
                // otherwise ignore the seek.
                public void onStopTrackingTouch(SeekBar s) {
                    SongModel thisSong = getCurrentSong();

                    updateSeekBar = true;

                    if (seekingSong == thisSong) {
                        setSecondsElapsed(s.getProgress());
                    }
                }
            });
        } else {
            // Clicking on the layout goes to NowPlayingActivity.
            v.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    NowPlayingActivity.show(mActivity);
                }
            });
        }

        return v;
    }

    /**
     * NowPlayingFragment
     * Use this to post Runnables to work off thread
     */
    public Handler getUIThreadHandler() {
        return uiThreadHandler;
    }

    /**
     * TODO: bekijken wat waar naar toe moet
     * @param connected
     * @param postConnect
     * @param loginFailure
     */
    // Should only be called the UI thread.
    private void setConnected(boolean connected, boolean postConnect, boolean loginFailure) {
        Log.v(TAG, "setConnected(" + connected + ", " + postConnect + ", " + loginFailure + ")");

        // The fragment may no longer be attached to the parent activity.  If so, do nothing.
        if (!isAdded()) {
            return;
        }

        if (postConnect) {
            //TODO naar mainactivity verhuist.
//            clearConnectingDialog();
            if (!connected) {
                // TODO: Make this a dialog? Allow the user to correct the server settings here?
                try {
                    Toast.makeText(mActivity, getText(R.string.connection_failed_text), Toast.LENGTH_LONG).show();
                } catch (IllegalStateException e) {
                    // We are not allowed to show a toast at this point, but
                    // the Toast is not important so we ignore it.
                    Log.i(TAG, "Toast was not allowed: " + e);
                }
            }
        }
        if (loginFailure) {
            Toast.makeText(mActivity, getText(R.string.login_failed_text), Toast.LENGTH_LONG)
                    .show();
            //TODO werkend krijgen
//            new AuthenticationDialog()
//                    .show(mActivity.getSupportFragmentManager(), "AuthenticationDialog");
        }

        // Ensure that option menu item state is adjusted as appropriate.
        getActivity().supportInvalidateOptionsMenu();

        if (mFullHeightLayout) {
            nextButton.setEnabled(connected);
            prevButton.setEnabled(connected);
            shuffleButton.setEnabled(connected);
            repeatButton.setEnabled(connected);
        }

        if (!connected) {
            updateSongInfo(null, null);

            playPauseButton.setImageResource(R.drawable.presence_online); // green circle

            if (mFullHeightLayout) {
                albumArt.setImageResource(R.drawable.icon_album_noart_fullscreen);
                nextButton.setImageResource(0);
                prevButton.setImageResource(0);
                shuffleButton.setImageResource(0);
                repeatButton.setImageResource(0);
                //TODO controleren van positie van de functie updatePlayerDropDown
//                updatePlayerDropDown(Collections.<PlayerModel>emptyList(), null);
                artistText.setText(getText(R.string.disconnected_text));
                currentTime.setText("--:--");
                totalTime.setText("--:--");
                seekBar.setEnabled(false);
                seekBar.setProgress(0);
            } else {
                albumArt.setImageResource(R.drawable.icon_album_noart);
                mProgressBar.setEnabled(false);
                mProgressBar.setProgress(0);
            }
        } else {
            if (mFullHeightLayout) {
                nextButton.setImageResource(R.drawable.ic_action_next);
                prevButton.setImageResource(R.drawable.ic_action_previous);
                seekBar.setEnabled(true);
            } else {
                mProgressBar.setEnabled(true);
            }
        }
    }

    /**
     * NowPlayingFragment
     * @param playStatus
     */
    private void updatePlayPauseIcon(PlayStatus playStatus) {
        playPauseButton
                .setImageResource((playStatus == PlayStatus.play) ? R.drawable.ic_action_pause
                        : R.drawable.ic_action_play);
    }

    /**
     * NowPlayingFragment
     * @param shuffleStatus
     */
    private void updateShuffleStatus(ShuffleStatus shuffleStatus) {
        if (mFullHeightLayout && shuffleStatus != null) {
            shuffleButton.setImageResource(shuffleStatus.getIcon());
        }
    }

    /**
     * NowPlayingFragment
     * @param repeatStatus
     */
    private void updateRepeatStatus(RepeatStatus repeatStatus) {
        if (mFullHeightLayout && repeatStatus != null) {
            repeatButton.setImageResource(repeatStatus.getIcon());
        }
    }

    /**
     * TODO: controle
     */
    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume...");

        //mImageFetcher.addImageCache(mActivity.getSupportFragmentManager(), mImageCacheParams);

        // Start it and have it run forever (until it shuts itself down).
        // This is required so swapping out the activity (and unbinding the
        // service connection in onDestroy) doesn't cause the service to be
        // killed due to zero refcount.  This is our signal that we want
        // it running in the background.
        mActivity.startService(new Intent(mActivity, SqueezeService.class));

        if (mService != null) {
            maybeRegisterCallbacks(mService);
            updateUIFromServiceState();
        }

        if (new Preferences(mActivity).isAutoConnect()) {
            //TODO contorleren of het correct is dat dit naar mainactivity verhuist is.
//            mActivity.registerReceiver(broadcastReceiver, new IntentFilter(
//                    ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }



    /**
     * This is called when the service is first connected, and whenever the activity is resumed.
     */
    public void maybeRegisterCallbacks(@NonNull ISqueezeService service) {
        if (!mRegisteredCallbacks) {
            service.registerCallback(serviceCallback);//geschreven voor nowplayping
//            service.registerConnectionCallback(connectionCallback);
//            service.registerHandshakeCallback(handshakeCallback);
            service.registerMusicChangedCallback(musicChangedCallback);
//            service.registerPlayersCallback(playersCallback);
//            service.registerVolumeCallback(volumeCallback);
            mRegisteredCallbacks = true;
        }
    }

    // Should only be called from the UI thread.
    private void updateUIFromServiceState() {
        // Update the UI to reflect connection state. Basically just for
        // the initial display, as changing the prev/next buttons to empty
        // doesn't seem to work in onCreate. (LayoutInflator still running?)
        Log.d(TAG, "updateUIFromServiceState");
        boolean connected = isConnected();
        setConnected(connected, false, false);
        if (connected) {
            PlayerStateModel playerState = getPlayerState();
            updateSongInfo(playerState.getCurrentSong(), playerState.getPlayStatus());
            updatePlayPauseIcon(playerState.getPlayStatus());
            updateTimeDisplayTo(playerState.getCurrentTimeSecond(), playerState.getCurrentSongDuration());
            updateShuffleStatus(playerState.getShuffleStatus());
            updateRepeatStatus(playerState.getRepeatStatus());
            Log.d("DEBUG 7", playerState.toString());
        }
    }

    /**
     * NowPlayingFragment
     * @param secondsIn
     * @param secondsTotal
     */
    private void updateTimeDisplayTo(int secondsIn, int secondsTotal) {
        if (mFullHeightLayout) {
            if (updateSeekBar) {
                if (seekBar.getMax() != secondsTotal) {
                    seekBar.setMax(secondsTotal);
                    totalTime.setText(Util.formatElapsedTime(secondsTotal));
                }
                seekBar.setProgress(secondsIn);
                currentTime.setText(Util.formatElapsedTime(secondsIn));
            }
        } else {
            if (mProgressBar.getMax() != secondsTotal) {
                mProgressBar.setMax(secondsTotal);
            }
            mProgressBar.setProgress(secondsIn);
        }
    }

    /**
     * NowPlayingFragment
     * @param song
     * @param playStatus
     */
    // Should only be called from the UI thread.
    private void updateSongInfo(SongModel song, PlayStatus playStatus) {
        Log.v(TAG, "updateSongInfo " + song);
        if (song != null) {
            albumText.setText(song.getAlbumName());
            trackText.setText(song.getName());
            if (mFullHeightLayout) {
                artistText.setText(song.getArtist());
                if (song.isRemote()) {
//                    song
                    nextButton.setEnabled(false);
                    Util.setAlpha(nextButton, 0.25f);
                    prevButton.setEnabled(false);
                    Util.setAlpha(prevButton, 0.25f);
                    btnContextMenu.setVisibility(View.GONE);
                } else {
                    nextButton.setEnabled(true);
                    Util.setAlpha(nextButton, 1.0f);
                    prevButton.setEnabled(true);
                    Util.setAlpha(prevButton, 1.0f);
                    btnContextMenu.setVisibility(View.VISIBLE);
                }
            }

            JSONObject numberinfo = new JSONObject();
            try {
                numberinfo.put("title", song.getName());
                numberinfo.put("artist", song.getArtist());
                numberinfo.put("album", song.getAlbumName());
                if(playStatus == PlayStatus.play && playStatus != null){
                    //pauze
                    numberinfo.put("status","play");
                }else if(playStatus != PlayStatus.play && playStatus != null){
                    //play
                    numberinfo.put("status","pause");
                }else{
                    numberinfo.put("status","null");
                }

                numberinfo.put("btnnext", (song.isRemote()) ? false : true);
                numberinfo.put("btnprevious", (song.isRemote()) ? false : true);
            } catch (JSONException e) {
//                e.printStackTrace();
                Log.d("nowplaying:squeezer", e.toString());
                Log.d("nowplaying:squeezer", e.getMessage());
            }

//            shuffleButton.setEnabled(connected);
//            repeatButton.setEnabled(connected);
            Log.d("nowplaying:squeezer-json", numberinfo.toString());

            //Requires a new thread to avoid blocking the UI
            new SendToDataLayerThread(BluetoothService.DATA_CURRENT_SONG, numberinfo.toString()).start();
//            new SendMesage(BluetoothService.DATA_CURRENT_SONG, numberinfo.toString()).execute();
            Log.d("nowplaying:squeezer-", "na post");
        } else {
            albumText.setText("");
            trackText.setText("");
            if (mFullHeightLayout) {
                artistText.setText("");
                btnContextMenu.setVisibility(View.GONE);
            }

            JSONObject numberinfo = new JSONObject();
            try {
                numberinfo.put("title", getText(R.string.disconnected_text));
                numberinfo.put("artist", getText(R.string.disconnected_text));
                numberinfo.put("album", getText(R.string.disconnected_text));
                numberinfo.put("status","null");
                numberinfo.put("btnnext", false);
                numberinfo.put("btnprevious", false);
            } catch (JSONException e) {
//                e.printStackTrace();
                Log.d("nowplaying:squeezer", e.toString());
                Log.d("nowplaying:squeezer", e.getMessage());
            }

            Log.d("nowplaying:squeezer-json", numberinfo.toString());
            //Requires a new thread to avoid blocking the UI
//            new SendMesage(BluetoothService.DATA_CURRENT_SONG, numberinfo.toString()).execute();
            new SendToDataLayerThread(BluetoothService.DATA_CURRENT_SONG, numberinfo.toString()).start();
            Log.d("nowplaying:squeezer-", "na post");
        }

        updateAlbumArt(song);
    }

    /**
     * NowPlayingFragment
     * @param song
     */
    // Should only be called from the UI thread.
    private void updateAlbumArt(SongModel song) {
        if (song == null || !song.hasArtwork()) {
            if (mFullHeightLayout) {
                albumArt.setImageResource(song != null && song.isRemote()
                        ? R.drawable.icon_iradio_noart_fullscreen
                        : R.drawable.icon_album_noart_fullscreen);
            } else {
                albumArt.setImageResource(song != null && song.isRemote()
                        ? R.drawable.icon_iradio_noart
                        : R.drawable.icon_album_noart);
            }
            return;
        }

        // The image fetcher might not be ready yet.
        if (mImageFetcher == null) {
            return;
        }

        String path = song.getArtworkUrl(mService);
        path = path.replace("%3A", ":");
        path = path.replace("%2F", "/");
        path = path.replace("imageproxy/", "");
        path = path.replace("/image.jpg", "");
        path = path.replace("/image.png", "");
        Log.d("DEBUG 1", path);

        mImageFetcher.loadImage(path, albumArt);
    }

    /**
     * TODO ONDERZOEK
     * @param seconds
     * @return
     */
    private boolean setSecondsElapsed(int seconds) {
        if (mService == null) {
            return false;
        }
        return mService.setSecondsElapsed(seconds);
    }

    /**
     * TODO CONTTROLEER
     * @return
     */
    private PlayerStateModel getPlayerState() {
        if (mService == null) {
            return null;
        }
        return mService.getPlayerState();
    }



    /**
     * NowPlayingFragment
     * @return
     */
    private SongModel getCurrentSong() {
        PlayerStateModel playerState = getPlayerState();
        return playerState != null ? playerState.getCurrentSong() : null;
    }

    /**
     * TODO MainActivity Onderzoek
     * @return
     */
    private boolean isConnected() {
        if (mService == null) {
            return false;
        }
        return mService.isConnected();
    }

    /**
     * TODO onderzoek
     */
    @Override
    public void onPause() {
        Log.d(TAG, "onPause...");

//        clearConnectingDialog();
        mImageFetcher.closeCache();
//
//        if (new Preferences(mActivity).isAutoConnect()) {
//            mActivity.unregisterReceiver(broadcastReceiver);
//        }
//
//        if (mRegisteredCallbacks) {
//            mService.cancelSubscriptions(this);
//            mRegisteredCallbacks = false;
//        }
//
//        super.onPause();
    }

    /**
     * TODO: Onderzoek
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            if (serviceConnection != null) {
                mActivity.unbindService(serviceConnection);
            }
        }
    }

    /**
     * NowPlayingFragment
     * Builds a context menu suitable for the currently playing song.
     * <p/>
     * Takes the general song context menu, and disables items that make no sense for the song that
     * is currently playing.
     * <p/>
     * {@inheritDoc}
     *
     * @param menu
     * @param v
     * @param menuInfo
     */
    public void onCreateContextMenu(ContextMenu menu, View v,  ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.songcontextmenu, menu);

        menu.findItem(R.id.play_now).setVisible(false);
        menu.findItem(R.id.play_next).setVisible(false);
        menu.findItem(R.id.add_to_playlist).setVisible(false);

        menu.findItem(R.id.view_this_album).setVisible(true);
        menu.findItem(R.id.view_albums_by_song).setVisible(true);
        menu.findItem(R.id.view_songs_by_artist).setVisible(true);
    }

    /**
     * NowPlayingFragment
     * Handles clicks on the context menu.
     * @param item
     * @return boolean
     */
    public boolean onContextItemSelected(MenuItem item) {
        SongModel song = getCurrentSong();
        if (song == null || song.isRemote()) {
            return false;
        }
        ArtistModel Artist = new ArtistModel();
        /**
         * TODO: fragmenten aanmaken en goede maniet van inladen gebruiken
         */
        switch (item.getItemId()) {
            case R.id.download:
                mActivity.downloadItem(song);
                return true;
            case R.id.view_this_album:
//                SongsListFragment.show(getActivity(), song.getAlbum());
                return true;
            case R.id.view_albums_by_song:
                Artist.setId(song.getArtistId());
                Artist.setName(song.getArtist());
//                AlbumListFragment.show(getActivity(), Artist);
                return true;
            case R.id.view_songs_by_artist:
                Artist.setId(song.getArtistId());
                Artist.setName(song.getArtist());
//                SongsListFragment.show(getActivity(), Artist);
                return true;
            default:
                throw new IllegalStateException("Unknown menu ID.");
        }
    }

    /**
     * NowPlayingFragment
     */
    private final IServiceCallback serviceCallback = new IServiceCallback() {
        @Override
        public void onPlayStatusChanged(final String playStatusName) {
            uiThreadHandler.post(new Runnable() {
                @Override
                public void run() {
                    updatePlayPauseIcon(PlayStatus.valueOf(playStatusName));
                }
            });
        }

        @Override
        public void onShuffleStatusChanged(final boolean initial, final int shuffleStatusId) {
            uiThreadHandler.post(new Runnable() {
                @Override
                public void run() {
                    ShuffleStatus shuffleStatus = ShuffleStatus.valueOf(shuffleStatusId);
                    updateShuffleStatus(shuffleStatus);
                    if (!initial) {
                        Toast.makeText(mActivity, mActivity.getServerString(shuffleStatus.getText()), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        @Override
        public void onRepeatStatusChanged(final boolean initial, final int repeatStatusId) {
            uiThreadHandler.post(new Runnable() {
                @Override
                public void run() {
                    RepeatStatus repeatStatus = RepeatStatus.valueOf(repeatStatusId);
                    updateRepeatStatus(repeatStatus);
                    if (!initial) {
                        Toast.makeText(mActivity, mActivity.getServerString(repeatStatus.getText()),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        @Override
        public void onTimeInSongChange(final int secondsIn, final int secondsTotal) {
            NowPlayingFragment.this.secondsIn = secondsIn;
            NowPlayingFragment.this.secondsTotal = secondsTotal;
            uiThreadHandler.sendEmptyMessage(UPDATE_TIME);
        }

        @Override
        public void onPowerStatusChanged(final boolean canPowerOn, final boolean canPowerOff) {
            uiThreadHandler.post(new Runnable() {
                public void run() {
                    //TODO bekijken wat met de functie moet gebeuren
//                    updatePowerMenuItems(canPowerOn, canPowerOff);
                }
            });
        }

        @Override
        public Object getClient() {
            return NowPlayingFragment.this;
        }
    };


    /**
     * NowPlayingFragment
     */
    private final IServiceMusicChangedCallback musicChangedCallback  = new IServiceMusicChangedCallback() {
        @Override
        public void onMusicChanged(final PlayerStateModel playerState) {
            uiThreadHandler.post(new Runnable() {
                public void run() {
                    updateSongInfo(playerState.getCurrentSong(), playerState.getPlayStatus());
                }
            });
        }

        @Override
        public Object getClient() {
            return NowPlayingFragment.this;
        }
    };


    class SendToDataLayerThread extends Thread {
        String path;
        String message;

        @Override
        public void start(){
            Log.d("nowplaying:squeezer-test", "TEST");
            super.start();
//            run();
        }

        // Constructor to send a message to the data layer
        SendToDataLayerThread(String p, String msg) {
            path = p;
            message = msg;
        }

        public void run() {
            NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(googleClient).await();
            Log.d("nowplaying:squeezer-nodes", nodes.getNodes().toString());
            for (Node node : nodes.getNodes()) {
                Log.d("nowplaying:squeezer-node", node.toString());
                MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(googleClient, node.getId(), path, message.getBytes()).await();

                if (result.getStatus().isSuccess()) {
                    Log.d("nowplaying:squeezer-run", "Message: {" + message + "} sent to: " + node.getDisplayName());
                }
                else {
                    // Log an error
                    Log.d("nowplaying:squeezer-run", "ERROR: failed to send Message");
                }
            }
        }
    }

    private class SendMesage extends AsyncTask<String, Void, Void> {
        String path;
        String message;

        SendMesage(String p, String msg) {
            path = p;
            message = msg;
            Log.d("nowplaying:squeezer-start", "grgds");
        }

        @Override
        protected Void doInBackground(String... params) {
            NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(googleClient).await();
            Log.d("nowplaying:squeezer-nodes", nodes.getNodes().toString());
            for (Node node : nodes.getNodes()) {
                Log.d("nowplaying:squeezer-node", node.toString());
                MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(googleClient, node.getId(), path, message.getBytes()).await();

                if (result.getStatus().isSuccess()) {
                    Log.d("nowplaying:squeezer-run", "Message: {" + message + "} sent to: " + node.getDisplayName());
                }
                else {
                    // Log an error
                    Log.d("nowplaying:squeezer-run", "ERROR: failed to send Message");
                }
            }
            return null;
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
//        String message = "Live @ ASOT 650 stage";
//        JSONObject numberinfo = new JSONObject();
//        try {
//            numberinfo.put("title", "Live @ ASOT Main Stage");
//            numberinfo.put("artist", "Dash Berlin");
//            numberinfo.put("album", "ASOT 650 Utrecht");
//            numberinfo.put("status","play");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        Log.d("mobile:squeezer", numberinfo.toString());
//
//        //Requires a new thread to avoid blocking the UI
//        new SendToDataLayerThread(PATH_CURRENT, numberinfo.toString()).start();
    }

    // Placeholders for required connection callbacks
    @Override
    public void onConnectionSuspended(int cause) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }
}
