package stefan.de.bruin.squeezer.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Map;

import stefan.de.bruin.squeezer.R;
import stefan.de.bruin.squeezer.Util;
import stefan.de.bruin.squeezer.framework.ArtworkItem;

/**
 * Created by Stefan on 16-1-2015.
 */
public class AlbumModel extends ArtworkItem {

    private int image;
    private String name;
    private String artist;
    private int year;

    @Override
    public String getPlaylistTag() {
        return "album_id";
    }

    @Override
    public String getFilterTag() {
        return "album_id";
    }

    public String getName() {
        return this.name;
    }

    public String getArtist() {
        return artist;
    }

    public int getYear() {
        return year;
    }

    public AlbumModel setName(String title) {
        this.name = title;
        return this;
    }

    public void setYear(int y) {
        this.year = y;
    }

    public void setArtist(String artist) {
        artist = artist;
    }

    public AlbumModel(String albumId, String album) {
        setId(albumId);
        setName(album);
    }

    public AlbumModel(Map<String, String> record) {
        setId(record.containsKey("album_id") ? record.get("album_id") : record.get("id"));
        setName(record.get("album"));
        setArtist(record.get("artist"));
        setYear(Util.parseDecimalIntOrZero(record.get("year")));
        setArtwork_track_id(record.get("artwork_track_id"));
    }

    private AlbumModel(Parcel source) {
        setId(source.readString());
        name = source.readString();
        artist = source.readString();
        year = source.readInt();
        setArtwork_track_id(source.readString());
    }

    public static final Parcelable.Creator<AlbumModel> CREATOR = new Parcelable.Creator<AlbumModel>() {
        public AlbumModel[] newArray(int size) {
            return new AlbumModel[size];
        }

        public AlbumModel createFromParcel(Parcel source) {
            return new AlbumModel(source);
        }
    };



    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(name);
        dest.writeString(artist);
        dest.writeInt(year);
        dest.writeString(getArtwork_track_id());
    }

    @Override
    public String toString() {
        return "id=" + getId() + ", name=" + name + ", artist=" + artist + ", year=" + year;
    }

    public int getCover(){
        return R.drawable.btn_shuffle_album;
    }

    public void setCover(int cover){

    }
}
