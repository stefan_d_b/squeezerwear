package stefan.de.bruin.squeezer;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import stefan.de.bruin.squeezer.Models.AlbumModel;
import stefan.de.bruin.squeezer.listitems.AlbumListFragment;

/**
 * Created by Stefan on 16-1-2015.
 */
public class ListItemAdapter extends ArrayAdapter<AlbumModel> {

    private final Context context;
    private final List<AlbumModel> values;

    public ListItemAdapter(Context albumListFragment, int custom_drawer_item, List<AlbumModel> dataList) {
        super(albumListFragment, custom_drawer_item, dataList);
        this.context = albumListFragment;
        this.values = dataList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.list_item, parent, false);

        ImageView icon = (ImageView) rowView.findViewById(R.id.icon);

        TextView text1 = (TextView) rowView.findViewById(R.id.text1);
        TextView text2 = (TextView) rowView.findViewById(R.id.text2);

        AlbumModel item = (AlbumModel) this.values.get(position);
        if(item.getName() != ""){
            text1.setText(item.getName());
        }
        if(item.getArtist() != ""){
            text2.setText(item.getArtist() + " - " + item.getYear());
            text2.setVisibility(View.VISIBLE);
        }else{
            text2.setVisibility(View.GONE);
        }

        icon.setImageResource(item.getCover());

        return rowView;
    }
}
