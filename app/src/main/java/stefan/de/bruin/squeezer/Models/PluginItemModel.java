/*
 * Copyright (c) 2011 Kurt Aaholst <kaaholst@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stefan.de.bruin.squeezer.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Map;

import stefan.de.bruin.squeezer.Util;
import stefan.de.bruin.squeezer.framework.Item;

/**
 * Represents a single item in a plugin.
 */
public class PluginItemModel extends Item {

    private String name;

    @Override
    public String getName() {
        return name;
    }

    public PluginItemModel setName(String name) {
        this.name = name;
        return this;
    }

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Relative URL to the icon to use for this item.
     */
    private String image;

    /**
     * @return the absolute URL to the icon to use for this item
     */
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private boolean hasitems;

    public boolean isHasitems() {
        return hasitems;
    }

    public void setHasitems(boolean hasitems) {
        this.hasitems = hasitems;
    }

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PluginItemModel(Map<String, String> record) {
        setId(record.get("id"));
        name = record.containsKey("name") ? record.get("name") : record.get("title");
        description = record.get("description");
        type = record.get("type");
        image = record.get("image");
        hasitems = (Util.parseDecimalIntOrZero(record.get("hasitems")) != 0);
    }

    public static final Parcelable.Creator<PluginItemModel> CREATOR = new Parcelable.Creator<PluginItemModel>() {
        public PluginItemModel[] newArray(int size) {
            return new PluginItemModel[size];
        }

        public PluginItemModel createFromParcel(Parcel source) {
            return new PluginItemModel(source);
        }
    };

    private PluginItemModel(Parcel source) {
        setId(source.readString());
        name = source.readString();
        description = source.readString();
        type = source.readString();
        image = source.readString();
        hasitems = (source.readInt() != 0);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(type);
        dest.writeString(image);
        dest.writeInt(hasitems ? 1 : 0);
    }

    @Override
    public String toString() {
        return "id=" + getId() + ", name=" + name;
    }

}
