/*
 * Copyright (c) 2011 Kurt Aaholst <kaaholst@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stefan.de.bruin.squeezer.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Map;

import stefan.de.bruin.squeezer.framework.PlaylistItem;


public class YearModel extends PlaylistItem {

    @Override
    public String getPlaylistTag() {
        return "year_id";
    }

    @Override
    public String getFilterTag() {
        return "year";
    }

    public YearModel(Map<String, String> record) {
        setId(record.get("year"));
    }

    public static final Parcelable.Creator<YearModel> CREATOR = new Parcelable.Creator<YearModel>() {
        public YearModel[] newArray(int size) {
            return new YearModel[size];
        }

        public YearModel createFromParcel(Parcel source) {
            return new YearModel(source);
        }
    };

    private YearModel(Parcel source) {
        setId(source.readString());
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
    }

    @Override
    public String getName() {
        return getId();
    }

    @Override
    public String toString() {
        return "year=" + getId();
    }

}
