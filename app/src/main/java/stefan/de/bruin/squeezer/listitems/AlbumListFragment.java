package stefan.de.bruin.squeezer.listitems;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import stefan.de.bruin.squeezer.CustomDrawerAdapter;
import stefan.de.bruin.squeezer.DrawerItem;
import stefan.de.bruin.squeezer.ListItemAdapter;
import stefan.de.bruin.squeezer.Models.AlbumModel;
import stefan.de.bruin.squeezer.R;

/**
 * Created by Stefan on 14-1-2015.
 */
public class AlbumListFragment extends Fragment {
    ImageView ivIcon;
    TextView tvItemName;
    ListView listView;
    View miniplayer;


    List<AlbumModel> dataList;

    ListItemAdapter adapter;

    public AlbumListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.items_list, container,false);

//        tvItemName.setText(getArguments().getString(ITEM_NAME));

        //filtervalue
        TextView filtervalue = (TextView) view.findViewById(R.id.filtervalue);
        filtervalue.setText(getArguments().getString("itemName"));

        listView = (ListView) view.findViewById(R.id.item_list);
//        miniplayer = view.findViewById(R.id.now_playing_fragment);

        dataList = new ArrayList<AlbumModel>();

        String[] items = new String[] {
            "ASOT 650 moscow",
            "ASOT 650 yekaterinburg",
            "ASOT 650 utrecht",
            "ASOT 650 buenos aires",
            "ASOT 650 miami",
            "ASOT 600 den bosch",
            "ASOT 600 guatamala",
            "ASOT 600 kl",
            "ASOT 600 madrid",
            "ASOT 600 mexico",
            "ASOT 600 miami",
            "ASOT 600 minsk",
            "ASOT 600 new york",
            "ASOT 600 sofia",
            "UR 5",
            "mirage",
            "intens",
        };

        for( int i = 0; i < items.length - 1; i++)
        {
            String element = items[i];

            AlbumModel item = new AlbumModel("","");
            item.setName(element);
            item.setArtist("artiest");
            item.setCover(R.drawable.ic_albums);
            item.setYear(2014);

            dataList.add(item);
        }

        adapter = new ListItemAdapter(container.getContext(), R.layout.list_item, dataList);
//
//        mDrawerList.setAdapter(adapter);
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, items);

        listView.setAdapter(adapter);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

//                if(mLastFirstVisibleItem<firstVisibleItem)
//                {
//                    Log.i("SCROLLING DOWN", "TRUE");
//                    miniplayer.setVisibility(View.GONE);
//                }
//                if(mLastFirstVisibleItem>firstVisibleItem)
//                {
//                    Log.i("SCROLLING UP","TRUE");
//                    miniplayer.setVisibility(View.VISIBLE);
//                }
//                mLastFirstVisibleItem=firstVisibleItem;

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        return view;
    }
}