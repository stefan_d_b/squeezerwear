package stefan.de.bruin.squeezer;

import java.util.ArrayList;
import java.util.List;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import stefan.de.bruin.squeezer.service.ISqueezeService;

/**
 * Created by Stefan on 13-1-2015.
 */
public class CustomDrawerAdapter extends ArrayAdapter<DrawerItem> {

    Context context;
    List<DrawerItem> drawerItemList;
    int layoutResID;
    ISqueezeService mService;

    public CustomDrawerAdapter(Context context, int layoutResourceID,List<DrawerItem> listItems, ISqueezeService service) {
        super(context, layoutResourceID, listItems);
        this.context = context;
        this.drawerItemList = listItems;
        this.layoutResID = layoutResourceID;
        this.mService = service;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        DrawerItemHolder drawerHolder;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            drawerHolder = new DrawerItemHolder();

            view = inflater.inflate(layoutResID, parent, false);
            drawerHolder.ItemName = (TextView) view.findViewById(R.id.drawer_itemName);
            drawerHolder.icon = (ImageView) view.findViewById(R.id.drawer_icon);
            drawerHolder.spinner = (Spinner) view.findViewById(R.id.drawerSpinner);
            drawerHolder.title = (TextView) view.findViewById(R.id.drawerTitle);
            drawerHolder.headerLayout = (RelativeLayout) view.findViewById(R.id.headerLayout);
            drawerHolder.itemLayout = (RelativeLayout) view.findViewById(R.id.itemLayout);
            drawerHolder.spinnerLayout = (RelativeLayout) view.findViewById(R.id.spinnerLayout);

            view.setTag(drawerHolder);
        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();
        }

        DrawerItem dItem = (DrawerItem) this.drawerItemList.get(position);

        if (dItem.getSpinner()) {
            drawerHolder.headerLayout.setVisibility(LinearLayout.INVISIBLE);
            drawerHolder.itemLayout.setVisibility(LinearLayout.INVISIBLE);
            drawerHolder.spinnerLayout.setVisibility(LinearLayout.VISIBLE);

            final CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(context, android.R.layout.simple_spinner_dropdown_item, dItem.getSpinnerItems());
            drawerHolder.spinner.setAdapter(adapter);
            drawerHolder.spinner.setSelection(dItem.getActivePlayerID());

            drawerHolder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(context, "User Changed", Toast.LENGTH_SHORT).show();
                    if (!adapter.getItem(position).equals(mService.getActivePlayer())) {
                        Log.i("Adapter", "onNavigationItemSelected.setActivePlayer(" + adapter.getItem(position) + ")");
                        mService.setActivePlayer(adapter.getItem(position));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                    // TODO Auto-generated method stub
                }
            });
        } else if (dItem.getTitle() != null) {
            drawerHolder.headerLayout.setVisibility(LinearLayout.VISIBLE);
            drawerHolder.itemLayout.setVisibility(LinearLayout.INVISIBLE);
            drawerHolder.spinnerLayout.setVisibility(LinearLayout.INVISIBLE);
            drawerHolder.title.setText(dItem.getTitle());
        } else {
            drawerHolder.headerLayout.setVisibility(LinearLayout.INVISIBLE);
            drawerHolder.spinnerLayout.setVisibility(LinearLayout.INVISIBLE);
            drawerHolder.itemLayout.setVisibility(LinearLayout.VISIBLE);

            Log.d("single item name ", dItem.getItemName());
            drawerHolder.icon.setImageDrawable(view.getResources().getDrawable(dItem.getImgResID()));
            drawerHolder.ItemName.setText(dItem.getItemName());
        }
        return view;
    }

    private static class DrawerItemHolder {
        TextView ItemName, title;
        ImageView icon;
        RelativeLayout headerLayout, itemLayout, spinnerLayout;
        Spinner spinner;
    }
}