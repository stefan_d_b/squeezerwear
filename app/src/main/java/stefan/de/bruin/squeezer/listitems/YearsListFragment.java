package stefan.de.bruin.squeezer.listitems;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import stefan.de.bruin.squeezer.R;

/**
 * Created by Stefan on 14-1-2015.
 */
public class YearsListFragment extends Fragment {
    ImageView ivIcon;
    TextView tvItemName;
    ListView listView;


    public static final String IMAGE_RESOURCE_ID = "iconResourceID";
    public static final String ITEM_NAME = "itemName";

    public YearsListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.items_list, container,false);

        listView = (ListView) view.findViewById(R.id.item_list);

        String[] items = new String[] {
                "Diverse artiesten",
                "Acda en de munik",
                "aly & fila",
                "andrew rayel",
                "armin van buuren",
                "ruben de ronde",
                "dash berlin",
                "anouk",
                "gaia",
                "shogun",
                "emma hewit",
                "W&W",
                "bastille",
                "bingo players",
                "C2C",
                "Coldplay",
                "Dany Vera",
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, items);

        listView.setAdapter(adapter);
        return view;
    }
}