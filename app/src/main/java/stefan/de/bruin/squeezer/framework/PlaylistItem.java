package stefan.de.bruin.squeezer.framework;

public abstract class PlaylistItem extends Item implements FilterItem {

    /**
     * Fetches the tag that represents this item in a <code>playlistcontrol</code> command.
     *
     * @return the tag, e.g., "album_id".
     */
    abstract public String getPlaylistTag();


    /** @return The parameter to use in the <code>playlistcontrol</code> command for this item.
     */
    public String getPlaylistParameter() {
        return getPlaylistTag() + ":" + getId();
    }

    @Override
    public String getFilterParameter() {
        return getFilterTag() + ":" + getId();
    }
}
